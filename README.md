# Android Challenge for Zooplus #

### How do I get set up? ##

0. Starting from an Android Studio with the Bitbucket Plug-in already installed
1. Open Android Studio. In the Welcome Screen, click on "Check out Project from Version Control"
2. Select option 'bitbucket'
3. Select Repository URL with the link: https://Wittie@bitbucket.org/Wittie/android-challenge.git
4. Click ok and open the project
  - If there is an error with IML files, it can be ignored (They are config files, not source code)
5. The project should be able to compile and be installed now

Note: I used to test it the emulator with the server already running. In the class DownloadAsyncTask
the emulator address is hardcoded. If this project is to be tested in a real device, this must be changed.


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact