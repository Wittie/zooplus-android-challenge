package de.zooplus.androidchallenge.download;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InterruptedIOException;
import java.lang.reflect.Type;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.List;

import de.zooplus.androidchallenge.data.model.Product;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by pablo on 05/03/17.
 */

public class DownloadAsyncTask extends AsyncTask<Void, Void, byte[]> {

    private final OnDownloadedListener listener;

    public interface OnDownloadedListener {
        void onDownloadSuccess(List<Product> products);
        void onDownloadError(String error);

    }

    private static final String TAG = DownloadAsyncTask.class.getSimpleName();

//    private static final String API_ENDPOINT = "http://127.0.0.1:3000/";
    public static final String API_ENDPOINT = "http://10.0.2.2:3000/";

    private final OkHttpClient client;

    private Exception exception;

    public DownloadAsyncTask(OkHttpClient client, OnDownloadedListener listener) {
        this.listener = listener;
        this.client = client;
    }

    @Override
    protected byte[] doInBackground(Void... params) {

        if (isCancelled()) {
            cancelOkHttpClient(TAG);
            return null;
        }

        Request request = new Request.Builder().url(API_ENDPOINT + "products").tag(TAG).build();

        try {
            Response response = client.newCall(request).execute();
            return  response.body().bytes();
        } catch (InterruptedIOException | UnknownHostException e){
            exception = e;
            cancelOkHttpClient(TAG);
        } catch (Exception e){
            cancelOkHttpClient(TAG);
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(byte[] bytes) {
        super.onPostExecute(bytes);

        if (bytes == null || bytes.length < 0) {
            if (listener != null && exception != null) {
                listener.onDownloadError(exception.toString());
            }

            return;
        }

        if (listener != null) {
            try {
                final String jsonString = new String(bytes);
                List<Product> products = parseJsonString(jsonString);
                listener.onDownloadSuccess(products);
            } catch (JSONException e) {
                e.printStackTrace();
                listener.onDownloadError(e.getLocalizedMessage());
            }
        }
    }

    private List<Product> parseJsonString(String jsonString) throws JSONException {
        JSONArray productsArray = new JSONObject(jsonString).getJSONArray("products");
        Log.d(TAG, "parseJsonString: Downloaded and Parsed " + productsArray.length() + " products");
        Type collectionType = new TypeToken<Collection<Product>>(){}.getType();
        return new GsonBuilder().create().fromJson(productsArray.toString(), collectionType);
    }


    private void cancelOkHttpClient(String tag) {
        for (okhttp3.Call call : client.dispatcher().queuedCalls()) {
            if (tag.equals(call.request().tag())) call.cancel();
        }
        for (okhttp3.Call call : client.dispatcher().runningCalls()) {
            if (tag.equals(call.request().tag())) call.cancel();
        }
    }

}

