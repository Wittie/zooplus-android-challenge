package de.zooplus.androidchallenge;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import de.zooplus.androidchallenge.shopactivity.ShopActivity;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onStartButtonClicked(View startButton) {
        startActivity(new Intent(this, ShopActivity.class));
    }
}
