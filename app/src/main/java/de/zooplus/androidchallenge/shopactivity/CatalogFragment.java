package de.zooplus.androidchallenge.shopactivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.net.UnknownHostException;
import java.util.List;

import de.zooplus.androidchallenge.R;
import de.zooplus.androidchallenge.data.model.Product;
import de.zooplus.androidchallenge.databinding.FragmentCatalogBinding;
import de.zooplus.androidchallenge.download.DownloadAsyncTask;
import okhttp3.OkHttpClient;

/**
 * Created by plopez on 05/03/17.
 */
public class CatalogFragment extends Fragment implements DownloadAsyncTask.OnDownloadedListener{

    public static final String TAG = CatalogFragment.class.getSimpleName();

    CatalogRecyclerViewAdapter catalogRecyclerViewAdapter = new CatalogRecyclerViewAdapter();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewDataBinding itemBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_catalog, container, false);

        final FragmentCatalogBinding binding = (FragmentCatalogBinding) itemBinding;
        binding.catalogRecyclerView.setAdapter(catalogRecyclerViewAdapter);
        binding.catalogRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return itemBinding.getRoot();
    }

    private void refreshData() {
        new DownloadAsyncTask(new OkHttpClient(), this).execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshData();
    }

    @Override
    public void onDownloadSuccess(List<Product> products) {
        catalogRecyclerViewAdapter.setProducts(products);
    }

    @Override
    public void onDownloadError(String error) {
        if (error.contains(UnknownHostException.class.getSimpleName())) {
            createErrorDialog(R.string.error_unknown_host_title, R.string.error_unknown_host_text).show();
        } else {
            Log.e(TAG, "onErrorRetrievingInfo: " + error);
        }
    }

    public Dialog createErrorDialog(@StringRes int title, @StringRes int text) {
        return new AlertDialog.Builder(getContext())
                .setIcon(android.R.drawable.stat_notify_error)
                .setTitle(title)
                .setMessage(text)
                .setNegativeButton(R.string.dialog_close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().onBackPressed();
                    }
                })
                .create();
    }
}
