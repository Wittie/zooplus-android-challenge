package de.zooplus.androidchallenge.shopactivity;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import de.zooplus.androidchallenge.R;
import de.zooplus.androidchallenge.data.model.Product;


/**
 * Created by plopez on 05/03/17.
 */

public class CatalogRecyclerViewAdapter extends RecyclerView.Adapter<CatalogViewHolder> {

    private ArrayList<Product> products;

    EventBus eventBus = EventBus.getDefault();

    @Override
    public CatalogViewHolder onCreateViewHolder(ViewGroup parent, int viewTypeRes) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding itemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_catalog, parent, false);
        return new CatalogViewHolder(eventBus, itemBinding);
    }

    @Override
    public void onBindViewHolder(CatalogViewHolder holder, int position) {
        holder.bind(products.get(position));
    }

    @Override
    public int getItemCount() {
        return products != null ? products.size() : 0;
    }

    public void setProducts(List<Product> products) {
        this.products = (ArrayList<Product>) products;
        notifyDataSetChanged();
    }
}
