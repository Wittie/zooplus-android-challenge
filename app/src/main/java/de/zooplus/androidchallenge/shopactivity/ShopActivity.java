package de.zooplus.androidchallenge.shopactivity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import de.zooplus.androidchallenge.R;
import de.zooplus.androidchallenge.databinding.ActivityShopBinding;

public class ShopActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    public static final String TAG = ShopActivity.class.getSimpleName();

    private ViewPagerAdapter sectionsPagerAdapter;

    ActivityShopBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_shop);

        setSupportActionBar(binding.toolbar);

        sectionsPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        setUpTabs();

        // Set up the ViewPager with the sections adapter.
        binding.viewPager.setAdapter(sectionsPagerAdapter);
        binding.viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabLayout));
    }

    private void setUpTabs() {
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.title_fragment_catalog));
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(R.string.title_fragment_cart));
        binding.tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        binding.tabLayout.addOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        binding.viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

}
