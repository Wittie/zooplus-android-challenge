package de.zooplus.androidchallenge.shopactivity;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import de.zooplus.androidchallenge.R;
import de.zooplus.androidchallenge.data.model.Product;

/**
 * Created by plopez on 05/03/17.
 */

public class CartFragmentRecyclerViewAdapter extends RecyclerView.Adapter<CatalogViewHolder> {

    private ArrayList<Product> products = new ArrayList<>();

    EventBus eventBus = EventBus.getDefault();

    @Override
    public CatalogViewHolder onCreateViewHolder(ViewGroup parent, int viewTypeRes) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding itemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_catalog, parent, false);
        return new CatalogViewHolder(eventBus, itemBinding);
    }

    @Override
    public void onBindViewHolder(CatalogViewHolder holder, int position) {
        holder.bind(products.get(position));
    }

    @Override
    public int getItemCount() {
        return products != null ? products.size() : 0;
    }

    public void addProduct(Product product) {
        if (isNewProduct(product.getId())) {
            this.products.add(product);
            notifyItemChanged(products.size() - 1);
        }
    }

    private boolean isNewProduct(String productId) {
        final int size = products.size();
        for (int i = 0; i < size; i++) {
            if (products.get(i).getId().equals(productId)) {
                return false;
            }
        }

        return true;
    }
}
