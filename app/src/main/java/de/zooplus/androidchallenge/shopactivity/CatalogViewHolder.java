package de.zooplus.androidchallenge.shopactivity;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import de.greenrobot.event.EventBus;
import de.zooplus.androidchallenge.BR;
import de.zooplus.androidchallenge.R;
import de.zooplus.androidchallenge.data.model.Product;
import de.zooplus.androidchallenge.databinding.RowCatalogBinding;
import de.zooplus.androidchallenge.download.DownloadAsyncTask;

import static android.content.ContentValues.TAG;


/**
 * Created by plopez on 05/03/17.
 */

public class CatalogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final ViewDataBinding binding;

    private final EventBus eventBus;

    private Product associatedProduct;

    public CatalogViewHolder(EventBus eventBus, ViewDataBinding itemBinding) {
        super(itemBinding.getRoot());
        this.eventBus = eventBus;
        this.binding = itemBinding;
        this.binding.getRoot().setOnClickListener(this);
    }

    public void bind(Product data) {
        this.associatedProduct = data;
        binding.setVariable(BR.product, data);
        binding.executePendingBindings();

        if (binding instanceof RowCatalogBinding) {
            final String url = DownloadAsyncTask.API_ENDPOINT + data.getImage_base_url() + data.getId() + ".png";
            Log.d(TAG, "bind: " + url);
            final ImageView imageView = ((RowCatalogBinding) binding).iconRowCatalog;
            Picasso.with(itemView.getContext())
                    .load(url)
                    .placeholder(R.drawable.zooplus_logo)
                    .error(R.drawable.zooplus_logo)
                    .into(imageView);
        }
    }

    @Override
    public void onClick(View v) {
        eventBus.post(new OnProductClickedEvent(associatedProduct));
    }

    public static class OnProductClickedEvent {

        private final Product associatedProduct;

        public OnProductClickedEvent(Product associatedProduct) {
            this.associatedProduct = associatedProduct;
        }

        public Product getAssociatedProduct() {
            return associatedProduct;
        }
    }
}
