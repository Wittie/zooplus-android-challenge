package de.zooplus.androidchallenge.shopactivity;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.greenrobot.event.EventBus;
import de.zooplus.androidchallenge.R;
import de.zooplus.androidchallenge.databinding.FragmentCartBinding;

/**
 * Created by plopez on 05/03/17.
 */

public class CartFragment extends Fragment {

    EventBus eventBus = EventBus.getDefault();

    CartFragmentRecyclerViewAdapter cartFragmentRecyclerViewAdapter = new CartFragmentRecyclerViewAdapter();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewDataBinding itemBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart, container, false);

        final FragmentCartBinding binding = (FragmentCartBinding) itemBinding;
        binding.catalogRecyclerView.setAdapter(cartFragmentRecyclerViewAdapter);
        binding.catalogRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        eventBus.unregister(this);
    }


    @SuppressWarnings("unused")
    public void onEvent(CatalogViewHolder.OnProductClickedEvent event) {
        cartFragmentRecyclerViewAdapter.addProduct(event.getAssociatedProduct());
    }
}
