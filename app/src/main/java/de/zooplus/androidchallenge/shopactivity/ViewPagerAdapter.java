package de.zooplus.androidchallenge.shopactivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by plopez on 05/03/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public static final int TOTAL_NUMBER_OF_FRAGMENTS = 2;
    private CatalogFragment catalogFragment = new CatalogFragment();
    private CartFragment cartFragment = new CartFragment();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return catalogFragment;
        }

        if (position == 1) {
            return cartFragment;
        }

        return null;
    }

    @Override
    public int getCount() {
        return TOTAL_NUMBER_OF_FRAGMENTS;
    }
}